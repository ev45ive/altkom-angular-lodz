import { AuthConfig } from 'src/app/security/auth.service';

export const environment = {
  production: true,
  api_url: 'https://api.spotify.com/v1/search',
  authConfig: {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: '054713805fcd46bc8cd2733deaa813bf',
    redirect_uri: 'http://localhost:4200/',
    response_type: 'token'
  } as AuthConfig
};
