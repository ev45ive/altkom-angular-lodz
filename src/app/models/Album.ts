export interface Entity {
    id: string
    name: string
}

export interface Album extends Entity {
    images: AlbumImage[]
    artists?: Artist[]
}

export interface Artist extends Entity { }

export interface AlbumImage {
    url: string
    height?: number
    width?: number
}
export interface PagingObject<T> {
    items: T[]
    total?: number
}

export interface AlbumsResponse {
    albums: PagingObject<Album>
}