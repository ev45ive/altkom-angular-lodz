import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

export class AuthConfig {
  auth_url: string
  client_id: string
  response_type: string
  redirect_uri: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {

    const jsonToken = sessionStorage.getItem('token')
    if (jsonToken) {
      this.token = JSON.parse(jsonToken)
    }

    if (!this.token && location.hash) {
      const params = new HttpParams({
        fromString: location.hash
      })

      const token = params.get('#access_token')
      if (token) {
        location.hash = ''
        this.token = token
        sessionStorage.setItem('token', JSON.stringify(this.token))
      }
    }
  }

  authorize() {
    sessionStorage.removeItem('token')

    const {
      auth_url, client_id, redirect_uri, response_type
    } = this.config

    const params = new HttpParams({
      fromObject: {
        client_id, redirect_uri, response_type
      }
    })
    const url = `${auth_url}?${params.toString()}`
    location.href = (url)
  }

  getToken() {
    if (!this.token) {
      this.authorize()
    }
    return this.token
  }
}
