import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Playlist } from 'src/app/models/Playlist';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit {

  @Input() playlist: Playlist

  @Output() save = new EventEmitter<Playlist>();

  // savePlaylist(draft: Pick<Playlist,'name'|'color'|'favorite'>) {
  savePlaylist(draft: Partial<Playlist>) {

    this.save.emit({
      ...this.playlist,
      ...draft
    })
  }

  constructor() { }

  ngOnInit() {
  }
}
