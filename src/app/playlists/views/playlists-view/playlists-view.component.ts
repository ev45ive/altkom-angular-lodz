import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode = 'show' // 'edit

  edit() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'show'
  }

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits!',
      favorite: true,
      color: '#ff00ff'
    },
    {
      id: 234,
      name: 'Angular TOP20!',
      favorite: false,
      color: '#ffff00'
    }, {
      id: 345,
      name: 'Best of Angular!',
      favorite: true,
      color: '#00ffff'
    }
  ]
  selected: Playlist = this.playlists[2]

  updatePlaylist(draft: Playlist) {
    const index = this.playlists.findIndex(p => p.id === draft.id)
    this.playlists.splice(index, 1, draft)
    this.selected = (draft)
    this.mode = 'show'
  }

  constructor() { }

  ngOnInit() {
  }

}
