import { Injectable, Inject, EventEmitter } from '@angular/core';
import { Album, AlbumsResponse } from 'src/app/models/Album';
import { API_URL_TOKEN } from './tokens';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/security/auth.service';
import { pluck, map, catchError, startWith, mergeAll, concatAll, concatMap, switchAll, switchMap, exhaustMap, exhaust } from 'rxjs/operators'
import { empty, throwError, of, concat, Subject, ReplaySubject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MusicSearchService {

  albumChanges = new BehaviorSubject<Album[]>([])

  private queryChanges = new BehaviorSubject<string>('batman')
  public query$ = this.queryChanges.asObservable()

  constructor(
    @Inject(API_URL_TOKEN)
    private search_api_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {
    (window as any).subject = this.albumChanges


    this.queryChanges.pipe(
      map(query => ({
        type: 'album',
        q: query
      })),
      switchMap(params => this.http.get<AlbumsResponse>(this.search_api_url, {
        headers: {
          Authorization: `Bearer ${this.auth.getToken()}`
        },
        params
      })),
      map(resp => resp.albums.items),
      catchError((error, caught) => {
        if (error instanceof HttpErrorResponse
          && error.status == 401) {
          this.auth.authorize()
        }
        return throwError(new Error(error.error.error.message))
      }))
      .subscribe({
        next: albums => {
          this.albumChanges.next(albums)
        }
      })
  }

  search(query: string) {
    this.queryChanges.next(query)
  }

  getAlbums() {
    return this.albumChanges.asObservable()
  }

  albums: Album[] = [
    {
      id: '5ty6b56h54ge',
      name: 'hasselhof from service',
      images: [
        {
          url: 'http://place-hoff.com/300/300'
        }
      ]
    },
  ]
}
