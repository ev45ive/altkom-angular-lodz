import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MusicSearchViewComponent } from './views/music-search-view/music-search-view.component';

/* /music/* */
const routes: Routes = [
  {
    path: '',
    component: MusicSearchViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
